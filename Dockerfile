FROM debian:bookworm-slim

MAINTAINER Ryan Rempel <rgrempel@cmu.ca>

ENV REFRESHED_AT=2024-07-23 \
    DEBIAN_FRONTEND=noninteractive \
    RTPENGINE_VERSION=11.5 \
    DEBIAN_VERSION=bookworm

RUN apt-get -qq update && apt-get -qq -y --no-install-recommends install \
    wget ca-certificates

# rtpengine repo
RUN wget https://dfx.at/rtpengine/latest/pool/main/r/rtpengine-dfx-repo-keyring/rtpengine-dfx-repo-keyring_1.0_all.deb && \
    dpkg -i rtpengine-dfx-repo-keyring_1.0_all.deb && \
    echo "deb [signed-by=/usr/share/keyrings/dfx.at-rtpengine-archive-keyring.gpg] https://dfx.at/rtpengine/$RTPENGINE_VERSION $DEBIAN_VERSION main" > /etc/apt/sources.list.d/dfx.at-rtpengine.list

RUN apt-get -qq update && apt-get -qq -y --no-install-recommends install \
    rtpengine gettext keepalived sngrep

ENTRYPOINT ["/usr/sbin/rtpengine"]
